package io.threatrix;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class WaterAmountServiceTest {

    private final WaterAmountService waterAmountService = new WaterAmountService();

    @Test
    public void testCase0() {
        int[] landscape = new int[]{5, 2, 3, 4, 4};
        int expect = 3;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void testCase1() {
        int[] landscape = new int[]{5, 2, 3, 1, 3, 2, 0, 4, 4};
        int expect = 13;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void testCase2() {
        int[] landscape = new int[]{4, 2, 1, 1, 3};
        int expect = 5;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void taskCase() {
        int[] landscape = new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1};
        int expect = 9;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void stepsCase() {
        int[] landscape = new int[]{1, 5, 2, 3, 4, 3, 2, 5, 4, 0, 3, 1};
        int expect = 14;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void ascendingProgression() {
        int[] landscape = new int[]{1, 2, 3, 4, 5};
        int expect = 0;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void descendingProgression() {
        int[] landscape = new int[]{5, 4, 3, 2, 1};
        int expect = 0;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void slide() {
        int[] landscape = new int[]{1, 1, 5, 5, 7, 7, 6, 6, 3, 3, 1, 1};
        int expect = 0;

        long actualFirst = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void emptyLandscape() {
        int[] landscape = new int[]{};
        int expect = 0;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void oneElementLandscape() {
        int[] landscape = new int[]{1};
        int expect = 0;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void twoElementLandscape() {
        int[] landscape = new int[]{2, 7};
        int expect = 0;

        long actualFirst = waterAmountService.calculateWaterAmountFirstEdition(landscape);
        long actualSecond = waterAmountService.calculateWaterAmountSecondEdition(landscape);
        long actualThird = waterAmountService.calculateWaterAmountThirdEdition(landscape);
        assertEquals(expect, actualFirst);
        assertEquals(expect, actualSecond);
        assertEquals(expect, actualThird);
    }

    @Test
    public void invalidLandscapeTooMuchValues() {
        int[] landscape = IntStream.range(0, 32_001).toArray();
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountFirstEdition(landscape));
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountSecondEdition(landscape));
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountThirdEdition(landscape));
    }

    @Test
    public void invalidLandscapeNegativeValue() {
        int[] landscape = new int[]{5, -1, 3, 4, 5, 4, 0, 3, 1};
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountFirstEdition(landscape));
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountSecondEdition(landscape));
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountThirdEdition(landscape));
    }

    @Test
    public void invalidLandscapeGreaterValue() {
        int[] landscape = new int[]{5, 1, 3, 4, 5, 32_001, 0, 3, 1};
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountFirstEdition(landscape));
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountSecondEdition(landscape));
        assertThrowsExactly(IllegalArgumentException.class, () -> waterAmountService.calculateWaterAmountThirdEdition(landscape));
    }
}
