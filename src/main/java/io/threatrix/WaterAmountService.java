package io.threatrix;

public class WaterAmountService {

    /**
     * Task constraints
     * <p>
     * 1. Max number of positions is 32000
     * 2. Height is between 0 and 32000.
     */
    private static final int MAX_VALUE = 32_000;

    public long calculateWaterAmountThirdEdition(int[] landscape) {

        checkLength(landscape);
        int maxHeightIndex = 0;

        // looking for the global maximum
        for (int i = 0; i < landscape.length; i++) {
            checkHeight(landscape[i]);
            if (landscape[i] >= landscape[maxHeightIndex]) {
                maxHeightIndex = i;
            }
        }

        int waterAmount = 0;

        // for the left side the second maximum will be fist element
        int tmpSecondMaxIndex = 0;

        // count the amount of water on the left side of the global maximum
        for (int i = 1; i < maxHeightIndex; i++) {
            int tmpWater = Math.min(landscape[tmpSecondMaxIndex], landscape[maxHeightIndex]) - landscape[i];
            if (tmpWater > 0) {
                waterAmount += tmpWater;
            }
            if (landscape[i] > landscape[tmpSecondMaxIndex]) {
                tmpSecondMaxIndex = i;
            }
        }

        // for the right side the second maximum will be last element
        tmpSecondMaxIndex = landscape.length - 1;

        // count the amount of water on the right side of the global maximum
        for (int i = landscape.length - 2; i > maxHeightIndex; i--) {
            int tmpWater = Math.min(landscape[tmpSecondMaxIndex], landscape[maxHeightIndex]) - landscape[i];
            if (tmpWater > 0) {
                waterAmount += tmpWater;
            }
            if (landscape[i] > landscape[tmpSecondMaxIndex]) {
                tmpSecondMaxIndex = i;
            }
        }
        return waterAmount;
    }

    public long calculateWaterAmountSecondEdition(int[] landscape) {

        checkLength(landscape);

        int waterAmount = 0;
        // water can't accumulate if landscape size is less than or equal 2
        if (landscape.length == 0 || landscape.length == 1 || landscape.length == 2) {
            return waterAmount;
        }
        int[] leftMax = new int[landscape.length];
        int tmpMax = 0;

        /**
         * 1. Check that an element has a valid value
         * 2. Looking for the maximum value on the left side for each element
         */
        for (int i = 0; i < landscape.length - 1; i++) {
            checkHeight(landscape[i]);
            if (landscape[i] > tmpMax) {
                tmpMax = landscape[i];
            }
            leftMax[i] = tmpMax;
        }

        tmpMax = 0;
        /**
         * 1. Looking for the maximum value on the right side
         * 2. Determine the water level
         *    (water level is minimum left or right maximum value for this element)
         * 3. Count the amount of water for element
         * 4. Increase final result by this value
         */
        for (int i = landscape.length - 1; i >= 0; i--) {
            if (landscape[i] > tmpMax) {
                tmpMax = landscape[i];
            }
            int waterLevel = Math.min(leftMax[i], tmpMax);
            if (waterLevel > landscape[i]) {
                waterAmount += waterLevel - landscape[i];
            }
        }
        return waterAmount;
    }

    public long calculateWaterAmountFirstEdition(int[] landscape) {
        int maxHeightIndex = 0;
        int previousHeightIndex = 0;
        int waterAmount = 0;
        int tempWaterAmount = 0;

        checkLength(landscape);
        // water can't accumulate if landscape size is less than or equal 2
        if (landscape.length == 0 || landscape.length == 1 || landscape.length == 2) {
            return waterAmount;
        }

        // Looking for the first maximum on the left side
        while (landscape[previousHeightIndex] >= landscape[maxHeightIndex]) {
            maxHeightIndex = previousHeightIndex;
            previousHeightIndex++;
            if (previousHeightIndex == landscape.length)
                return waterAmount;
        }

        for (int i = previousHeightIndex; i < landscape.length; i++) {
            checkHeight(landscape[i]);
            if (landscape[i] >= landscape[maxHeightIndex]) {
                // if we found a height greater than the maximum
                // then we fix all the potential water and continue
                waterAmount += tempWaterAmount;
                tempWaterAmount = 0;
                maxHeightIndex = i;
                previousHeightIndex = i + 1;
                continue;
            } else {
                // collect potential water
                tempWaterAmount += landscape[maxHeightIndex] - landscape[i];
                // we've taken step up, so we can collect some potential water
                if (landscape[i] > landscape[i - 1]) {
                    int collWater = 0;
                    int endIndex = landscape[i] > landscape[previousHeightIndex] ? maxHeightIndex : previousHeightIndex;
                    int rightMax = landscape[i - 1];
                    // go back to previous height or to maximum height
                    // and calculate amount of collected water
                    for (int j = i - 1; j > endIndex; j--) {
                        collWater += landscape[i] - Math.max(landscape[j], rightMax);
                        if (landscape[j] > rightMax) {
                            rightMax = landscape[j];
                        }
                    }
                    // fix the collected water and reduce the amount of potential water
                    waterAmount += collWater;
                    tempWaterAmount -= collWater;
                }
            }

            // change previous maximum
            if (landscape[i] > landscape[previousHeightIndex]) {
                previousHeightIndex = i;
            }
        }
        return waterAmount;
    }

    /**
     * Landscape size check.
     * <p>
     * Constraint of the task - the maximum landscape size is {@value #MAX_VALUE}
     */
    private static void checkLength(int[] landscape) {
        if (landscape.length > MAX_VALUE) {
            throw new IllegalArgumentException("Maximum landscape size is " + MAX_VALUE);
        }
    }

    /**
     * Landscape value check.
     * <p>
     * Constraint of the task - each landscape value is between 0 and {@value #MAX_VALUE}.
     */
    private static void checkHeight(int value) {
        if (value < 0 || value > MAX_VALUE) {
            throw new IllegalArgumentException("Each landscape value is between 0 and " + MAX_VALUE);
        }
    }
}
